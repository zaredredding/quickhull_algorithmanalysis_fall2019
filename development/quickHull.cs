﻿using System;
using System.Collections.Generic;

namespace quickHull_Implementation
{
    class QuickHull
    {
        public enum Side { Left, Right, OnLine };

        public static void Main(string[] args)
        {
            //Generates a new random on program start.
            //Since Random is pseudo-random, I generate ONE, then pass 
            //that instance into each point to achieve optimal variety
            Random rand = new Random();





            //Create a hull specific to the assigned Points array next
            List<Point> assignedHull = new List<Point>();
            //New array of Point objects containing the ordered pairs given in the Assignment
            Point[] assignedPoints = {
                                        new Point(1, 6), new Point(4, 15), new Point(7, 7), new Point(10, 13),
                                        new Point(11, 6), new Point(11, 18), new Point(11, 21), new Point(12, 10),
                                        new Point(15, 18), new Point(16, 6), new Point(18, 3), new Point(18, 12),
                                        new Point(19, 15), new Point(22, 19)
                                     };

            Console.WriteLine("Assigned Point Array Contents: \n" + PointArrayToString(assignedPoints, 4));
            Console.WriteLine("Assigned Point Array Convex Hull Points: \n" + ComputeConvexHull(assignedHull, assignedPoints));





            //Create a hull specific to the random Points array next
            List<Point> randomHull = new List<Point>();
            //Create a new Array of randomly generated Point objects
            Point[] randomPoints = new Point[14];
            for (int i = 0; i < randomPoints.Length; i++)
                randomPoints[i] = new Point(rand, 0, 26);

            Console.WriteLine("Random Point Array Contents: \n" + PointArrayToString(randomPoints, 4));
            Console.WriteLine("Random Point Array Convex Hull Points: \n" + ComputeConvexHull(randomHull, randomPoints));






            //Keep Console open at the end of the program until user input is detected.
            Console.ReadKey();
        }


        private static string ComputeConvexHull(List<Point> hull, Point[] set)
        {
            string hullString = "";

            if (set.Length < 3)
                return "Hull is not possible with a set containing less than 3 points.";

            //Find the minimum and maximum X positions in the set
            Point min_XPoint = set[0];
            Point max_XPoint = set[0];
            foreach(Point p in set)
            {
                if (p.X < min_XPoint.X)
                    min_XPoint = p;

                if (p.X > max_XPoint.X)
                    max_XPoint = p;
            }

            //Find hull points on Right side of the Line
            QuickHullAlgorithm(hull, set, min_XPoint, max_XPoint, Side.Right);

            //Do the same for the Left side of the line
            QuickHullAlgorithm(hull, set, min_XPoint, max_XPoint, Side.Left);

            //Build a string to represent the ConvexHull
            hullString += PointArrayToString(hull.ToArray(), 4);

            return hullString;
        }


        /// <summary>
        /// Builds a convex hull using the quickHull Algorithm
        /// </summary>
        private static void QuickHullAlgorithm(List<Point> hull, Point[] set, Point linePointA, Point linePointB, Side side)
        {
            //Find the point farthest away from the given line.
            Point nextHullPoint = GetFarthestPoint(set, linePointA, linePointB, side);

            //If there are no points farther away, then our current lines
            //endpoints are a part of the hull.
            if (nextHullPoint == null)
            {
                bool containsPointA = false;
                bool containsPointB = false;

                foreach (Point p in hull)
                {
                    if (p == linePointA)
                        containsPointA = true;

                    if (p == linePointB)
                        containsPointB = true;
                }

                if(!containsPointA)
                    hull.Add(linePointA);

                if(!containsPointB)
                    hull.Add(linePointB);
            }
            //Otherwise recursively work throught the rest of the set splitting the data each time.
            else
            {
                QuickHullAlgorithm(hull, set, nextHullPoint, linePointA, ((SegregatePoint(nextHullPoint, linePointA, linePointB) == Side.Right) ? Side.Left : Side.Right) );
                QuickHullAlgorithm(hull, set, nextHullPoint, linePointB, ((SegregatePoint(nextHullPoint, linePointB, linePointA) == Side.Right) ? Side.Left : Side.Right) );
            }
        }


        #region quickHull Helper Methods

        /// <summary>
        /// Given a set of points, and a line, return the point farthest from said line.
        /// </summary>
        /// <param name="set"> Set of points </param>
        /// <param name="linePointA"> Current Lines Start/End Point </param>
        /// <param name="linePointB"> Current Lines Respective other of linePointA </param>
        /// <param name="side"> Side we want to check on </param>
        /// <returns></returns>
        private static Point GetFarthestPoint(Point[] set, Point linePointA, Point linePointB, Side side)
        {
            Point farthestPoint = null;

            int farthestDist = 0;

            foreach (Point p in set)
            {
                int curDist = Math.Abs(DistanceFromLine(linePointA, linePointB, p));

                if ((SegregatePoint(linePointA, linePointB, p) == side) && (curDist > farthestDist))
                {
                    farthestDist = curDist;
                    farthestPoint = p;
                }
            }

            return farthestPoint;
        }


        /// <summary>
        /// Returns the side of the line the given point should be considered on.
        /// </summary>
        /// <param name="linePointA"> Current Lines Start/End Point </param>
        /// <param name="linePointB"> Current Lines Respective other of linePointA </param>
        /// <param name="pointToSegregate"> Point to decide upon </param>
        private static Side SegregatePoint(Point linePointA, Point linePointB, Point pointToSegregate)
        {
            int currentDist = DistanceFromLine(linePointA, linePointB, pointToSegregate);

            if (currentDist == 0)
                return Side.OnLine;

            Side side = (currentDist > 0) ? Side.Right : Side.Left;

            return side;
        }


        /// <summary>
        /// Computes the distance in 2-D Coordinate space of a given point, from a given line and its endpoints
        /// </summary>
        /// <param name="linePointA"> Current Lines Start/End Point </param>
        /// <param name="linePointB"> Current Lines Respective other of linePointA </param>
        /// <param name="pointToSegregate"> Point to calculate distance with </param>
        private static int DistanceFromLine(Point linePointA, Point linePointB, Point pointToCompare)
        {
            return (pointToCompare.Y - linePointA.Y) * (linePointB.X - linePointA.X) - (linePointB.Y - linePointA.Y) * (pointToCompare.X - linePointA.X);
        }

        #endregion

        #region Program Helper Methods

        /// <summary>
        /// Builds a formatted string of Points from a given Point Array.
        /// </summary>
        /// <param name="pointArray"> array to format </param>
        /// <param name="pointsPerLine"> ordered pairs per line </param>
        private static string PointArrayToString(Point[] pointArray, int pointsPerLine)
        {
            string composition = "";

            int count = 0;
            for (int i = 0; i < pointArray.Length; i++)
            {
                if (count < pointsPerLine)
                {
                    if (i < pointArray.Length - 1)
                    {
                        composition += "( " + pointArray[i].X + ", " + pointArray[i].Y + " ), ";
                    }
                    else
                    {
                        composition += "( " + pointArray[i].X + ", " + pointArray[i].Y + " ). \n\n";
                    }

                    count++;
                }
                else
                {
                    composition += "\n";
                    count = 0;
                    i--;
                }
            }

            return composition;
        }

        #endregion
    }
}
