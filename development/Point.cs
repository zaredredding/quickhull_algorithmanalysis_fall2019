﻿using System;

namespace quickHull_Implementation
{
    class Point
    {
        private int x;
        private int y;

        public int X
        {
            get { return this.x; }
            private set { }
        }
        public int Y
        {
            get { return this.y; }
            private set { }
        }

        /// <summary>
        /// Constructs a new point on a 2-D coordinate system containing randomly generated components x, and y.
        /// </summary>
        /// <param name="rand"> Random Number Generator </param>
        /// <param name="min"> Minimum Value (Inclusive) </param>
        /// <param name="max"> Maximum Value (Exclusive) </param>
        public Point(Random rand, int min, int max)
        {
            this.x = rand.Next(min, max);
            this.y = rand.Next(min, max);
        }

        /// <summary>
        /// Constructs a new point on a 2-D coordinate system given components x, and y.
        /// </summary>
        /// <param name="x"> X-Value of the Ordered Pair </param>
        /// <param name="y"> Y-Value of the Ordered Pair </param>
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
